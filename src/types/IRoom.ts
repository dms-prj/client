export interface IRoom {
    uid: string;
    number: string;
    dormitory: number;
    gender: string;
    freespace: number;
    capacity: number;
};
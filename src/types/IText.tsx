export interface IText{
    id: number;
    title: string;
    subtitle: string;
}
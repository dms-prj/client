import {filterState} from "../store/reducers/filterSlice";

export interface IFilter {
    page: number,
    perPage: number,
    building?: string,
    room?: string,
    params?: filterState
}
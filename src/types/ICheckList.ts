export interface ICheckList {
    fluorography: boolean;
    clean: boolean;
    no_arrears: boolean;
    corpse: boolean;
    documents: boolean;
    no_disciplinary: boolean;
}
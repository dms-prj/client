import {FC} from "react";

export interface IGuideItem{
    id: number;
    title: string;
    content: FC;
}
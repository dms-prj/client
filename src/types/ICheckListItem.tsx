import { FC } from "react";
import { ICheckList } from "./ICheckList";

export interface ICheckListItem {
    id: number;
    title: string;
    description: FC;
    label: keyof ICheckList;
}

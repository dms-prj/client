import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { campusApi } from "../services/campus";
import { msgApi } from "../services/msgs";
import checkItemReducer from "./reducers/checkItemSlice";
import feedbackDialogReducer from "./reducers/feedbackDialogSlice";
import authorizationReducer from "./reducers/authorizationSlice";
import authorizationDialogReducer from "./reducers/authorizationDialogSlice";
import registrationReducer from "./reducers/registrationSlice";
import navbarReducer from "./reducers/navbarSlice";
import thunk from "redux-thunk";
import filterReducer from "./reducers/filterSlice";
import pagesReducer from "./reducers/pagesSlice";
import {loginAPI} from "../services/auth_and_registr";
import searchReducer from "./reducers/searchSlice";
import {checklistApi} from "../services/checklist";

const rootReducer = combineReducers({
    checkItemReducer,
    authorizationReducer,
    authorizationDialogReducer,
    registrationReducer,
    feedbackDialogReducer,
    navbarReducer,
    filterReducer,
    pagesReducer,
    searchReducer,
    [checklistApi.reducerPath]: checklistApi.reducer,
    [msgApi.reducerPath]: msgApi.reducer,
    [campusApi.reducerPath]: campusApi.reducer,
    [loginAPI.reducerPath]: loginAPI.reducer
});

export const setupStore = () => {
    return configureStore({
        reducer: rootReducer,
        devTools: true,
        middleware: (getDefaultMiddleware) =>
            getDefaultMiddleware().concat(msgApi.middleware, campusApi.middleware, loginAPI.middleware, checklistApi.middleware, thunk),
    });
};

export type RootState = ReturnType<typeof rootReducer>;
export type AppStore = ReturnType<typeof setupStore>;
export type AppDispatch = AppStore['dispatch'];
setupListeners(setupStore().dispatch);

import {IUser} from "../../types/IUser";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {AuthResponse} from "../../services/auth_and_registr";

export enum AuthStatus {
    STARTED,
    NOT_AUTHORIZED,
    AUTHORIZED,
    FAILED
}

export interface AuthorizationState {
    isAuth: boolean;
    isFailed: boolean;
    userName?: string;
    isStarted: boolean;
    isDone: boolean;
}

const initialState: AuthorizationState = {
    isAuth: false,
    isFailed: false,
    isStarted: false,
    isDone: false
}

export const authorizationSlice = createSlice( {
    name: 'authorization',
    initialState,
    reducers: {
        logout: state => {
            state.isAuth = false;
            state.isFailed = false;
            state.userName = undefined;
            localStorage.removeItem("accessToken");
            localStorage.removeItem("userName");
        },
        //login_success: (state: AuthorizationState, action: PayloadAction<{userName: string, accessToken: string}>) => {
        login_success: (state: AuthorizationState, action: PayloadAction<AuthResponse>) => {
            localStorage.setItem("accessToken", action.payload.access_token)
            state.userName = action.payload.name;
            localStorage.setItem("userName", action.payload.name);
            state.isAuth = true;
            state.isFailed= false;
           // console.log("isDone at success: ", state.isDone);
        },
        login_started: state => {
            state.isStarted = true;
            state.isDone = false;
            state.isFailed = false;
            //console.log("isDone at started: ", state.isDone);
        },
        login_done: state => {
            state.isStarted = false;
            state.isDone = true;
            //console.log("isDone at done: ", state.isDone);
        },
        login_failed_already_exist: state => {
            state.isFailed = true;
            //console.log("isDone at failf: ", state.isDone);
        }
    }
})

export const {login_success, logout, login_done, login_started, login_failed_already_exist} = authorizationSlice.actions;
export default authorizationSlice.reducer;
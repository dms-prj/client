import {createSlice, PayloadAction} from '@reduxjs/toolkit'

interface AuthDialogState {
    isOpen: boolean,
    to?: string
}

const initialState: AuthDialogState = {
    isOpen: false,
    to: '/'
};

export const authorizationDialogSlice = createSlice({
    name: 'authorizationDialog',
    initialState,
    reducers: {
        openAuthDialog: (state, action: PayloadAction<{to?: string}>) => {
            state.isOpen = true;
            if (!(action.payload.to === undefined))
            {
                state.to = action.payload.to;
            }
        },
        closeAuthDialog: state => {
            state.isOpen = false;
            state.to = undefined;
        },
        redirectAuthDialog: state => {
            state.isOpen = false;
        }
    }
});

export const {openAuthDialog, closeAuthDialog, redirectAuthDialog} = authorizationDialogSlice.actions;
export default authorizationDialogSlice.reducer;
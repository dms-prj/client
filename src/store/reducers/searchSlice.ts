import {createSlice, PayloadAction} from "@reduxjs/toolkit";

export interface SearchState {
    building?: string,
    room?: string,
    isClicked: boolean,
};

const initialState: SearchState = {
    isClicked: false
}

export const searchSlice = createSlice({
    name: 'search',
    initialState,
    reducers: {
        setBuilding: (state, action: PayloadAction<string>) => {
            if (action.payload !== "")
                state.building = action.payload;
            else
                state.building = undefined;
        },
        setRoom: (state, action: PayloadAction<string>) => {
            if (action.payload !== "")
                state.room = action.payload;
            else
                state.room = undefined;
        },
        click_search: state => {
            state.isClicked = true;
        },
        stop_search: state => {
            state.isClicked = false;
        },
        setSearchState: (state, action: PayloadAction<{building?: string, room?: string}>) => {
            if (action.payload.building === "")
                state.building = undefined;
            else
                state.building = action.payload.building;
            if (action.payload.room === "")
                state.room = undefined;
            else
                state.room = action.payload.room;
        }
    }
})

export const {setBuilding, setRoom, click_search, stop_search, setSearchState} = searchSlice.actions;
export default searchSlice.reducer;
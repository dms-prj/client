import { createSlice } from '@reduxjs/toolkit'

export interface DialogState {
    isOpen: boolean;
}

const initialState: DialogState = {
    isOpen: false
}

export const feedbackDialogSlice = createSlice({
    name: 'feedbackDialog',
    initialState,
    reducers: {
        openFeedbackDialog: state => {
            state.isOpen = true
        },
        closeFeedbackDialog: state => {
            state.isOpen = false
        }
    }
});

export const {openFeedbackDialog, closeFeedbackDialog} = feedbackDialogSlice.actions;
export default feedbackDialogSlice.reducer;
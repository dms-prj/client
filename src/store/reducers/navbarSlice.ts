import {createSlice, PayloadAction} from '@reduxjs/toolkit'

export enum NavTab {
    GUIDE,
    CHECKLIST,
    MAP
}

export interface NavbarState {
    tab: NavTab;
}

const initialState: NavbarState = {
    tab: NavTab.GUIDE
}

export const navbarSlice = createSlice({
    name: 'navbar',
    initialState,
    reducers: {
        updateNavbar: (state, action: PayloadAction<NavTab>) => {
            state.tab = action.payload;
        }
    }
});

export const {updateNavbar} = navbarSlice.actions;
export default navbarSlice.reducer;
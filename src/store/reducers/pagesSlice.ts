import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface pagesState {
    page: number;
    per_page: number;
    total: number;
    total_page: number;
};

const initialState: pagesState = {
    page: 0,
    per_page: 5,
    total: 20,
    total_page: 4,
};

export const pagesSlice = createSlice({
    name: 'pages',
    initialState,
    reducers: {
        setPage: (state, action: PayloadAction<number>) => {
            state.page = action.payload;
        },
        setPerPage: (state, action: PayloadAction<React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>>) => {
            state.per_page = parseInt(action.payload.target.value, 10);
            state.page = 0;
            state.total_page = Math.ceil(state.total / state.per_page);
        },
        setPerPageNumber: (state, action: PayloadAction<number>) => {
            state.per_page = action.payload;
            state.page = 0;
            state.total_page = Math.ceil(state.total / state.per_page);
        },
        setTotal: (state, action: PayloadAction<number>) => {
            state.total = action.payload;
            // нужно?
            state.total_page = Math.ceil(state.total / state.per_page);
        },
    },
});

export const { setPage, setPerPage, setTotal, setPerPageNumber } = pagesSlice.actions;
export default pagesSlice.reducer;
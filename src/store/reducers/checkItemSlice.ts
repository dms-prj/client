import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICheckList } from "../../types/ICheckList";

export interface checkItemState {
    checked: ICheckList;
    selected: number;
};

const initialState: checkItemState = {
    checked: {
        fluorography: false,
        clean: false,
        no_arrears: false,
        corpse: false,
        documents: false,
        no_disciplinary: false,
    },
    selected: -1,
};

export const checkItemSlice = createSlice({
    name: 'checkItem',
    initialState,
    reducers: {
        setCheckList: (state, action: PayloadAction<ICheckList>) => {
            state.checked = action.payload;
        },
        check: (state, action: PayloadAction<keyof ICheckList>) => {
            state.checked[action.payload] = !state.checked[action.payload];
        },
        choose: (state, action: PayloadAction<number>) => {
            state.selected = (action.payload === state.selected) ?
                -1 : action.payload;
        }
    }
});

export const { check, choose, setCheckList } = checkItemSlice.actions;
export default checkItemSlice.reducer;

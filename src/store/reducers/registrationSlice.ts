import {IUser} from "../../types/IUser";
import {createSlice} from "@reduxjs/toolkit";

export enum RegistrationStatus {
    STARTED,
    NOT_REGISTERED,
    REGISTERED,
    FAILED
}

interface RegistrarionState {
    isRegistered: RegistrationStatus;
    user?: IUser;
}

const initialState: RegistrarionState = {
    isRegistered: RegistrationStatus.NOT_REGISTERED
}

export const registrationSlice = createSlice( {
    name: 'registration',
    initialState,
    reducers: {
        registration_started: state => {
            state.isRegistered = RegistrationStatus.STARTED;
        },
        // НУЖНА ЛИ?
        registration_success: state => {
            state.isRegistered = RegistrationStatus.REGISTERED;
        },
        registration_failed: state => {
            state.isRegistered = RegistrationStatus.FAILED;
        },
        registration_done: state => {
            state.isRegistered = RegistrationStatus.NOT_REGISTERED;
        }
    }
})

export const {registration_success, registration_failed, registration_started, registration_done} = registrationSlice.actions;
export default registrationSlice.reducer;
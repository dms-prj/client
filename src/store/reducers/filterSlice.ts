import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface filterState {
    gender?: string;
    roomType?: string;
    freePlaces: {        
        onePlace: boolean;
        twoPlaces: boolean;
        threePlaces: boolean;
    }
};

const initialState: filterState = {
    gender: 'any-gender',
    roomType: 'any-room-type',
    freePlaces: {        
        onePlace: true,
        twoPlaces: true,
        threePlaces: true,
    }
};

export const filterSlice = createSlice({
    name: 'filter',
    initialState,
    reducers: {
        changeGender: (state, action: PayloadAction<React.ChangeEvent<HTMLInputElement>>) => {
            state.gender = action.payload.target.value;
        },

        changeTypeOfRoom: (state, action: PayloadAction<React.ChangeEvent<HTMLInputElement>>) => {
            state.roomType = action.payload.target.value;
        },

        chooseFreePlaces: (state, action: PayloadAction<React.ChangeEvent<HTMLInputElement>>) => {
            state.freePlaces[action.payload.target.name as keyof typeof state.freePlaces] = action.payload.target.checked;
        },
        setFilterState: (state, action: PayloadAction<filterState>) => {
            state.gender = action.payload.gender;
            state.roomType = action.payload.roomType;
            state.freePlaces = action.payload.freePlaces;
        }
    }
});

export const { changeGender, changeTypeOfRoom, chooseFreePlaces, setFilterState } = filterSlice.actions;
export default filterSlice.reducer;
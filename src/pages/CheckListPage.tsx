import { FC } from "react";
import { Outlet } from "react-router-dom";
import { Container, Divider, Typography } from "@mui/material";
import CheckList from "../components/CheckList";
import Grid from "@mui/material/Unstable_Grid2";

const CheckListPage: FC = () => {

    return (
        <Container maxWidth="lg" sx={{ mt: 16 }}>
            <Typography variant="h4" align="center" gutterBottom>
                Чек-лист переселения
            </Typography>
            <Typography variant="h5" color="textSecondary" align="center" gutterBottom>
                Личный список требований, которые нужно выполнить или проверить для переселения
            </Typography>
            <Divider sx={{ borderBottomWidth: 5, mt: 2 }} />
            <Grid container spacing={6} my={6}>
                <Grid lg={6} display="flex" alignItems="flex-start">
                    <CheckList />
                </Grid>
                <Grid lg display="flex" justifyContent="flex-end" alignItems="flex-start">
                    <Outlet />
                </Grid>                
            </Grid>
        </Container>
    );
};

export default CheckListPage;

import React, {FC, useEffect} from 'react';
import {Box, Button, Grid, Link, TextField, Typography} from "@mui/material";
import {IText} from "../types/IText";
import {RegistationPageText} from "../text/RegistrationPageText";
import {useAppDispatch, useAppSelector} from "../hooks/redux";
import {useNavigate} from "react-router-dom";
import {openAuthDialog} from "../store/reducers/authorizationDialogSlice";
import {registration_done, registration_failed, RegistrationStatus} from "../store/reducers/registrationSlice";
import {loginAPI, RegistrationRequest} from "../services/auth_and_registr";
import {login_done, login_started, login_success} from "../store/reducers/authorizationSlice";


const RegistrationPage: FC = () => {

    const textPage: IText = RegistationPageText[0];
    const textName: IText = RegistationPageText[1];
    const textEmail: IText = RegistationPageText[2];
    const textPassword: IText = RegistationPageText[3];
    const textLogIn: IText = RegistationPageText[4];
    const textSigningButton: string = "Зарегистрироваться";

    const registrationStatus = useAppSelector((state) => state.registrationReducer.isRegistered);
    const url_to = useAppSelector((state) => state.authorizationDialogReducer.to);
    const [trigger] = loginAPI.useRegistrationMutation();

    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const result: RegistrationRequest = {
            email: data.get('email')!.toString(),
            password: data.get('password')!.toString(),
            name: data.get('name')!.toString()
        }

        dispatch(login_started());
        trigger.call({}, result).unwrap().then((response) => {
            //console.log("response: ", response);
            dispatch(login_success({name: result.name, access_token: response as string, token_type: 'bearer'}));
            dispatch(login_done());
            handleSuccessfulRegistration();
        }).catch((error) => {

            console.log("error: ", error);
            // TODO проверка на ошибку существующего юзера
            if (true)
            {
                dispatch(registration_failed());
            }
            else {
                console.log("strange error at registration: ", error.data.detail.msg);
            }

            dispatch(login_done());
        });
    }

    const handleSuccessfulRegistration = () => {
        handleClose();
    };

    const handleLinkToAuthDialog = () => {
        dispatch(openAuthDialog({to: url_to}));
        navigate(-1);
    }

    const handleClose = () => {
        if (!(url_to === undefined))
            navigate('/' + url_to);
        else
            navigate(-1);
    }

    return (
        <Box display="flex" alignItems="center" justifyContent="center" width="100%" height="100%" position="fixed" top="0" left="0">
            <Box component="form" onSubmit={handleSubmit} display="flex" flexDirection="column" alignItems="center" width="450px">
                <Typography variant="h5"> {textPage.title} </Typography>
                {(!(registrationStatus === RegistrationStatus.FAILED)) ?
                    null
                    :
                    <Typography variant="subtitle1" color="red" sx={{marginTop: "8px"}}> {textPage.subtitle} </Typography>
                }
                <TextField
                    name="name"
                    error={(registrationStatus === RegistrationStatus.FAILED)}
                    label={textName.title}
                    required
                    variant="outlined"
                    color="secondary"
                    sx={{ width: "100%", marginTop: "16px"}}/>
                <TextField
                    name="email"
                    error={(registrationStatus === RegistrationStatus.FAILED)}
                    label={textEmail.title}
                    required
                    variant="outlined"
                    color="secondary"
                    sx={{ width: "100%", marginTop: "16px"}}/>
                <TextField
                    name="password"
                    required
                    label={textPassword.title}
                    variant="outlined"
                    color="secondary"
                    error={(registrationStatus === RegistrationStatus.FAILED)}
                    sx={{ width: "100%", marginTop: "16px"}}/>
                <Button
                    color="secondary"
                    type="submit"
                    variant="contained"
                    sx={{  marginTop: "32px", marginBottom: "24px"}}
                    fullWidth
                    size="large"> {textSigningButton} </Button>
                <Link
                    align="right"
                    underline="hover"
                    color="secondary"
                    alignSelf="flex-end"
                    component="button"
                    onClick={handleLinkToAuthDialog}
                >
                    {textLogIn.title}
                </Link>
            </Box>
        </Box>
    );
};

export default RegistrationPage;
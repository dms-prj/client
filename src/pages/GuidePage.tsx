import React, { FC } from 'react';
import { Box, Typography } from "@mui/material";
import { IText } from "../types/IText";
import { GuidePageInfoText } from "../text/GuidePageInfoText";
import GuideStepper from "../components/GuideStepper";
import { GuideStepperText } from "../text/GuideStepperText";
import cl from "./GuidePage.module.css";

const  GuidePage: FC = () => {

    const infoBlockMain : IText = GuidePageInfoText[0];
    const infoBlockGuide : IText = GuidePageInfoText[1];

    return (
        <Box sx={{ marginTop: 7 }}>
            <Box display="flex" alignItems="center" flexDirection="column">
                <Box display="flex" alignItems="center" flexDirection="column" width="100%" >
                    <Box className={cl.top}> 
                        <Box className={cl.topText}>                     
                            <Typography 
                                variant="h3" 
                                sx={{ marginTop: { xs: 3, md: 10 }, color: "white", fontSize: { xs: '38px', md: '48px' } }}
                            > 
                                {infoBlockMain.title} 
                            </Typography>
                            <Typography 
                                variant="subtitle1"
                                sx={{ marginTop: { xs: 1, md: 3 }, color: "white", maxWidth: { xs: "80%", md: "100% "}, lineHeight: {xs: 1.5, md: 1.75}}}
                            > 
                                {infoBlockMain.subtitle} 
                            </Typography>
                        </Box>                        
                    </Box>
                    <Box display="flex" alignItems="center" flexDirection="column" sx={{ paddingTop: "50px", maxWidth: { xs: "90%", md: "100% "}}}>
                        <Typography 
                            variant="h4" 
                            sx={{ fontSize: { xs: "26px", md: "34px" }}}
                        > 
                            {infoBlockGuide.title}
                        </Typography>
                        <Typography 
                            align="center" 
                            variant="h5" 
                            sx={{ marginTop: { xs: "10px", md: "20px"}, fontSize: { xs: "18px", md: "24px" } }}
                        > 
                            {infoBlockGuide.subtitle} 
                        </Typography>
                    </Box>
                </Box>
                <GuideStepper sx={{ marginTop: { xs: "25px", md: "60px"} }} guideArray={GuideStepperText} />
            </Box>
        </Box>

    );
};

export default GuidePage;
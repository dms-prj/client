import { FC } from "react";
import { Box, Typography, Link, Card, CardContent } from "@mui/material";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

const ErrorPage: FC = () => {

    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));

    return (
        <Box 
            display="flex" 
            flexDirection="column"
            alignItems="center" 
            justifyContent="center"
            sx={{width: "100vw", height: "100vh"}}
        >
            <Card sx={{ maxWidth: "90%", borderRadius: "10px", padding: {xs: 2, md: 4} }}>
                <CardContent sx={{display: "flex", flexDirection: "column", alignItems: "center"}}>
                    <Typography gutterBottom variant={matches? "h3" : "h4"} sx={{ textAlign: "center" }}>
                        Nothing to see here!
                    </Typography>
                    <Link href="/" underline="hover" sx={{ fontSize: "1.5rem", textAlign: "center" }}>Go to the home page</Link>
                </CardContent>                
            </Card>            
        </Box>
    )
};

export default ErrorPage;
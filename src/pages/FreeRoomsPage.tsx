import { FC } from "react";
import { Container, Divider, Typography } from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import Search from "../components/UI/Sidebar/Search";
import FilterParams from "../components/UI/Sidebar/FilterParams";
import RoomList from "../components/RoomList";

const FreeRoomsPage: FC = () => {
    
    return (
        <Container maxWidth="lg" sx={{ mt: 16 }}>
            <Typography variant="h4" align="center" gutterBottom>
                Свободные места
            </Typography>
            <Typography variant="h5" color="textSecondary" align="center" gutterBottom>
                Здесь вы можете найти себе подходящую комнату для переселения
            </Typography>
            <Divider sx={{ borderBottomWidth: 5, mt: 2 }} />
            <Grid container spacing={4} sx={{ mt: 2 }}>
                <Grid md={4}>
                    <Search />
                    <FilterParams />
                </Grid>
                <Grid md={8}>
                    <RoomList />
                </Grid>                
            </Grid>
        </Container>
    );
};

export default FreeRoomsPage;
import {BaseQueryFn, FetchArgs, fetchBaseQuery, FetchBaseQueryError} from "@reduxjs/toolkit/dist/query/react";

const host = process.env.REACT_APP_API_HOST;
const port = process.env.REACT_APP_API_PORT;

export const baseQueryWithAuthToken = fetchBaseQuery({
    baseUrl: `http://${host}:${port}/`,
    prepareHeaders: headers => {
        const current_token = localStorage.getItem('accessToken');
        if (current_token) {
            headers.set('authorization', `Bearer ${current_token}`)
        }
        return headers;
    }
})

export const baseQueryWithReauth: BaseQueryFn<
    string | FetchArgs,
    unknown,
    FetchBaseQueryError
    > = async (args, api, extraOptions) => {
    let result = await baseQueryWithAuthToken(args, api, extraOptions)
    // TODO: раскоммитить, когда у сервера появится опция рефреша
    /*if (result.error && result.error.status === 401) {
        // try to get a new token
        const refreshResult = await baseQueryWithAuthToken('/refresh', api, extraOptions)
        if (refreshResult.data) {
            // store the new token
            localStorage.setItem('accessToken', refreshResult.data as string)
            // retry the initial query
            result = await baseQueryWithAuthToken(args, api, extraOptions)
        } else {
            // TODO: не получилось рефрешнуться
            // api.dispath(loggedOut)
        }
    }*/
    return result
}
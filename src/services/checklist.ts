import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import type { ICheckList } from "../types/ICheckList";
import {baseQueryWithAuthToken} from "./baseQueryFunctions";

interface IUpdate {
  name: string;
  state: boolean;
};

export const checklistApi = createApi({
    reducerPath: 'checklistApi',
    baseQuery: baseQueryWithAuthToken,
    endpoints: (build) => ({
      getCheckList: build.query<ICheckList, void>({
        query: (body) => ({
          url: 'checklist',
          method: 'GET',
          body,
        }),
      }),
      updateCheckList: build.mutation<void, IUpdate>({
        query: updates => ({
          url: 'checklist',
          method: 'PUT',
          body: {name: updates.name, state: updates.state},
        }),
      }),
    }),
});

export const { useGetCheckListQuery, useUpdateCheckListMutation } = checklistApi;
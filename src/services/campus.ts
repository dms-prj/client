import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { IRoom } from '../types/IRoom';
import {IFilter} from "../types/IFilter";
import {makeListParams} from "./parsing_url/parsing_url";
import {baseQueryWithAuthToken} from "./baseQueryFunctions";


const host = process.env.REACT_APP_API_HOST;
const port = process.env.REACT_APP_API_PORT;

interface RoomsResponse {
  rooms: IRoom[];
  total: number;
  page: number;
  perPage: number;
}

export interface ListParams {
//  page?: number | void;
  limit?: number | void;
  filter: IFilter;
};

export const campusApi = createApi({
    reducerPath: 'campusApi',
    tagTypes: ['Rooms'],
    baseQuery: fetchBaseQuery({
      baseUrl: `http://${host}:${port}/`,
      prepareHeaders: headers => {
        headers.set('Access-Control-Allow-Origin', '*')
        return headers;
      }
  }),
    endpoints: (build) => ({
        listRooms: build.query<RoomsResponse, ListParams>({
          query: ({limit = 5, filter}) => ({
              url: 'campus',
              params: makeListParams({limit, filter})
          }),
          providesTags: (result) =>
            result
              ? [
                  ...result.rooms.map(({ uid }) => ({ type: 'Rooms' as const, uid })),
                  { type: 'Rooms', id: 'LIST' },
                ]
              : [{ type: 'Rooms', id: 'LIST' }],
      }),
    })
});

export const { useListRoomsQuery } = campusApi;
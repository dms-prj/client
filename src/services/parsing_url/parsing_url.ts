import {IFilter} from "../../types/IFilter";
import {ListParams} from "../campus";

// удаление из UrlSearchParams ключей со значением "undefined"
export const makeURLsearchFromParams = (params: URLSearchParams) => {
    let keys: string[] = [];
    params.forEach((value, key, parent) => {
        if (value === "undefined")
            keys.push(key);
    });
    keys.map(key => {
        params.delete(key);
    })
}

// TODO: проверка корректности параметров??? руками можно вбить кривые параметры и всё сломать!
export const parseUrlParamsToFilter = (url_search: string): IFilter => {
    const params = new URLSearchParams(url_search);

    // могут ли они быть undefined/null?
    const page = params.get("page");
    const perPage = params.get("perPage");
    //
    const building = params.get("dormitory");
    const room = params.get("number");
    const gender = params.get("gender");
    const roomType = params.get("capacity");
    const freeplaces_one = params.get("onePlace");
    const freeplaces_two = params.get("twoPlaces");
    const freeplaces_three = params.get("threePlaces");

    return {
        page: Number(page!) - 1,
        perPage: Number(perPage!),
        building: (building !== null) ? building : undefined,
        room: (room !== null) ? room : undefined,
        params: {
            gender: (gender === null) ? 'any-gender' : gender,
            roomType: (roomType === null) ? 'any-room-type' : roomType,
            freePlaces: {
                onePlace: (freeplaces_one !== null),
                twoPlaces: (freeplaces_two !== null),
                threePlaces: (freeplaces_three !== null)
            }
        }
    }
}

// нужен для вызова api query
export const makeListParams = (listParams: ListParams) : Record<any, any> => {
    const default_page = 1;
    const params: IListParams = {
        perPage: (listParams.limit) ? listParams.limit : undefined,
        page: (listParams.filter.page) ? listParams.filter.page : default_page,
        dormitory: (listParams.filter.building) ? listParams.filter.building : undefined,
        number: (listParams.filter.room) ? listParams.filter.room : undefined,
        gender: (listParams.filter.params?.gender === 'any-gender') ?
                    undefined : listParams.filter.params?.gender,
        capacity: (listParams.filter.params?.roomType === 'any-room-type') ? 
                    undefined : listParams.filter.params?.roomType,
        onePlace: (listParams.filter.params?.freePlaces.onePlace) ? true : false,
        twoPlaces: (listParams.filter.params?.freePlaces.twoPlaces) ? true : false,
        threePlaces: (listParams.filter.params?.freePlaces.threePlaces) ? true : false
    }
    return params;
}


export interface IListParams {
    perPage?: number,
    page?: number,
    dormitory?: string,
    number?: string,
    gender?: string,
    capacity?: string,
    onePlace?: boolean,
    twoPlaces?: boolean,
    threePlaces?: boolean
}
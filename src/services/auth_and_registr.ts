import {createApi} from "@reduxjs/toolkit/query/react";
import {fetchBaseQuery} from "@reduxjs/toolkit/dist/query/react";

export interface LoginRequest {
    username: string,
    password: string
}

export interface RegistrationRequest {
    name: string,
    email: string,
    password: string
}

export interface ErrorDetail {
    location: {
        title: string
    }
    msg: string,
    type: string
}

export type LoginResponse = ErrorDetail | string;

export interface AuthResponse {
    access_token: string,
    token_type: string,
    name: string
}

const host = process.env.REACT_APP_API_HOST;
const port = process.env.REACT_APP_API_PORT;


export const loginAPI = createApi({
    reducerPath: 'loginAPI',
    baseQuery: fetchBaseQuery({
        baseUrl: `http://${host}:${port}/`
    }),
    endpoints: (build) => ({
        login: build.mutation<AuthResponse, LoginRequest>({
            query: (loginData) => ({
                url: 'auth/login',
                method: "POST",
                body: new URLSearchParams ({
                    'username': loginData.username,
                    'password': loginData.password
                    }),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }),
            extraOptions: {
                maxRetries: 2
            }
        }),
        registration: build.mutation<LoginResponse, RegistrationRequest>({
            query: (loginData) => ({
                url: 'auth/register',
                method: "POST",
                body: loginData
            }),
            extraOptions: {
                maxRetries: 2
            }
        })

        // TODO: раскоммитить, когда появится эта опция у сервера
        /*refresh: build.query<string, void>({
            query: () => ({
                url: '/refresh'
            }),
            extraOptions: {
                maxRetries: 2
            }
        }),*/
    })
})
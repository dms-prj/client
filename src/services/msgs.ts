import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import type { IMsg } from "../types/IMsg";

const host = process.env.REACT_APP_API_HOST;
const port = process.env.REACT_APP_API_PORT;

export const msgApi = createApi({
    reducerPath: 'msgApi',
    tagTypes: ['Msgs'],
    baseQuery: fetchBaseQuery({ baseUrl: `http://${host}:${port}/` }),
    endpoints: (build) => ({
      addMsg: build.mutation<IMsg, Partial<IMsg>>({
        query: (body) => ({
          url: 'feedback',
          method: 'POST',
          body,
        }),
        invalidatesTags: [{type: 'Msgs', id: 'LIST'}],
      }),
    }),
});

export const { useAddMsgMutation } = msgApi;
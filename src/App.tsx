import GuidePage from "./pages/GuidePage";
import {Routes, Route, Link, Navigate} from "react-router-dom";
import CheckListPage from './pages/CheckListPage';
import InfoItem from './components/InfoItem';
import Navbar from './components/UI/Navbar/Navbar';
import RegistrationPage from "./pages/RegistrationPage";
import {useEffect} from "react";
import RequireAuth from "./components/hoc/RequireAuth";
import Footer from "./components/Footer/Footer";
import ErrorPage from "./pages/ErrorPage";
import FreeRoomsPage from "./pages/FreeRoomsPage";
import {loginAPI} from "./services/auth_and_registr";
import {useAppDispatch, useAppSelector} from "./hooks/redux";
import {AuthStatus, login_done, login_started, login_success} from "./store/reducers/authorizationSlice";

function App() {

    const dispatch = useAppDispatch();

    // TODO: Раскоммитить, когда у сервера появится refresh
    //const [trigger] = loginAPI.useLazyRefreshQuery();
    /*useEffect(() => {
        if (localStorage.getItem('accessToken')) {
            dispatch(login_started());
            trigger.call({}).unwrap().then((payload) => {
                dispatch(login_success(payload));
                dispatch(login_done());
            }).catch((error) => {
                // TODO: ошибка?
                dispatch(login_done());
            });
        }
    }, []);*/

    const isAuth = useAppSelector(state => state.authorizationReducer.isAuth);

    // TODO: если будет нормальный рефреш, то убрать это и раскоммитить верхнюю версию
    useEffect(() => {
        if (localStorage.getItem('accessToken') && (!isAuth)) {
            // TODO: в этой версии рефреша неоткуда восстановить имя! Храню его в localStorage... Убрать его оттуда (authSlice) с приходом норм рефреша
            dispatch(login_success({ access_token: localStorage.getItem('accessToken')!, name: localStorage.getItem('userName')!, token_type: 'bearer' }));
            dispatch(login_done());
        }
    }, [])

    return(
        <Routes>
            <Route path="/" element={<><Navbar/><Footer/></>}>
                <Route index element={<GuidePage />} />
                <Route path="checklist" element={<CheckListPage/>}>
                    <Route path=":id" element={<InfoItem/>}/>
                </Route>
                <Route path="freerooms" element={<FreeRoomsPage />}/>
            </Route>
            <Route path="/registration" element={<RegistrationPage/>}/>
            <Route path="*" element={<ErrorPage/>} />
        </Routes>
    );
};

export default App;

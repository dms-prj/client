import {IGuideItem} from "../types/IGuideItem";
import Text0 from "./componentsForGuideStepper/Text0";
import Text1 from "./componentsForGuideStepper/Text1";
import Text2 from "./componentsForGuideStepper/Text2";
import Text3 from "./componentsForGuideStepper/Text3";
import Text4 from "./componentsForGuideStepper/Text4";
import Text5 from "./componentsForGuideStepper/Text5";

export const GuideStepperText: IGuideItem[] = [
    {
        id: 0,
        title: "Дождаться 1 октября",
        content: Text0
    },
    {
        id: 1,
        title: "Найти свободное место",
        content: Text1
    },
    {
        id: 2,
        title: "Взять заявление",
        content: Text2
    },
    {
        id: 3,
        title: "Оплатить проживание в общежитии на полгода вперёд",
        content: Text3
    },
    {
        id: 4,
        title: "Отдать заявление и дождаться окончания проверок",
        content: Text4
    },
    {
        id: 5,
        title: "Сдать постельные принадлежности и выданный инвентарь",
        content: Text5
    }
]
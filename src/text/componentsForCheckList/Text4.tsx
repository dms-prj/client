import { FC } from "react";
import { Typography } from "@mui/material";

const Text4: FC = () => {
    return (        
        <Typography 
            color="textSecondary" 
            sx={{ border: '3px dashed', borderRadius: 2, borderColor: 'divider', px:2, py:1 }}
        >
            Для подачи заявления в отдел поселения вам понадобится паспорт 
            или студенческий билет.
        </Typography>
    );
};

export default Text4;

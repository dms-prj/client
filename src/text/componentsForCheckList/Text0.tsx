import { FC } from "react";
import { Typography } from "@mui/material";

const Text0: FC = () => {
    return (        
        <Typography 
            color="textSecondary" 
            sx={{ border: '3px dashed', borderRadius: 2, borderColor: 'divider', px:2, py:1 }}
        >
            Для переселения необходимо иметь действующую справку о прохождении флюорографии. 
            Она действительна на протяжении 11 месяцев со дня выдачи. 
            <p>
                Её можно получить в 
                Николаевской больнице (Петергоф, ул. Константиновская, д. 1) в 107 кабинете.
            </p>
            Расписание:
            <ul>
                <li>понедельник, среда <b>8:30-17:00</b></li>
                <p></p>
                <li>вторник, четверг <b>11:00-19:00</b></li>
                <p></p>
                <li>пятница <b>8:30-16:00</b></li>
            </ul>
            <Typography color="textSecondary" variant="body2">
                * может измениться
            </Typography>
        </Typography>
    );
};

export default Text0;

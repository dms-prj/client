import { FC } from "react";
import { Typography, Link } from "@mui/material";

const Text2: FC = () => {
    return (        
        <Typography 
            color="textSecondary" 
            sx={{ border: '3px dashed', borderRadius: 2, borderColor: 'divider', px:2, py:1 }}
        >
            {"Необходимо зайти на сайт "} 
            <Link href="https://pay.spbu.ru" underline="hover" color="secondary">единого платёжного сервиса СПбГУ</Link>
            , залогиниться и убедиться, что во вкладке справа "заказы и счета"  
            нет задолженностей. Их можно оплатить двумя способами:
            <ul>
                <li>
                    В кассе на первом этаже физического факультета 
                    (ул. Ульяновская, 3, Санкт-Петербург),
                    оплата проходит сразу.
                </li>
                <p></p>
                <li>
                    {"Онлайн на этом же "} 
                    <Link href="https://pay.spbu.ru" underline="hover" color="secondary">сайте</Link>
                    , учитывайте, что оплата проходит в течение 7 дней.
                </li>
            </ul>
            <Typography color="textSecondary" variant="body2">
                * если у вас были многоразовые долги за оплату проживания вашу заявку могут отклонить
            </Typography>
        </Typography>
    );
};

export default Text2;

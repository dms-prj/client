import { FC } from "react";
import { Typography, Box } from "@mui/material";

const Text1: FC = () => {
    return (        
        <Typography 
            color="textSecondary" 
            sx={{ border: '3px dashed', borderRadius: 2, borderColor: 'divider', px:2, py:1 }}
        >
            Всё движимое имущество, принадлежащее СПбГУ, а также комната, 
            ванна и туалет должны быть в хорошем состоянии.
            Нужно прибраться и убедиться в отсутствии дефектов.
            <Box mt={1}>
                Комендант может попросить принести фотографии помещений.
            </Box> 
        </Typography>
    );
};

export default Text1;

import { FC } from "react";
import { Typography } from "@mui/material";

const Text5: FC = () => {
    return (        
        <Typography 
            color="textSecondary" 
            sx={{ border: '3px dashed', borderRadius: 2, borderColor: 'divider', px:2, py:1 }}
        >
            Убедитесь, что у вас нет дисциплинарных взысканий, иначе вам откажут в 
            переселении. 
            <Typography color="textSecondary" variant="body2" mt={1}>
                * в зависимости от степени нарушения, к вам могут быть 
                применены следующие дисциплинарные взыскания:
                <li>замечание</li>
                <li>выговор</li>
                <li>отчисление из университета</li>
            </Typography>
        </Typography>
    );
};

export default Text5;

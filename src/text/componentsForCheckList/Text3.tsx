import { FC } from "react";
import { Typography } from "@mui/material";

const Text3: FC = () => {
    return (        
        <Typography 
            color="textSecondary" 
            sx={{ border: '3px dashed', borderRadius: 2, borderColor: 'divider', px:2, py:1 }}
        >
            Недопустимо отсутствие в общежитии больше 30 дней подряд не в каникулярное 
            время за всю историю проживания.
        </Typography>
    );
};

export default Text3;

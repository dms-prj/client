import { ICheckListItem } from "../types/ICheckListItem";
import Text0 from "./componentsForCheckList/Text0";
import Text1 from "./componentsForCheckList/Text1";
import Text2 from "./componentsForCheckList/Text2";
import Text3 from "./componentsForCheckList/Text3";
import Text4 from "./componentsForCheckList/Text4";
import Text5 from "./componentsForCheckList/Text5";


export const checkListText: ICheckListItem[] = [
    {
        id: 0,
        title: 'Флюорография',
        description: Text0,
        label: "fluorography"
    },
    {
        id: 1,
        title: 'Чистый блок',
        description: Text1,
        label: "clean"
    },
    {
        id: 2,
        title: 'Нет задолженностей',
        description: Text2,
        label: "no_arrears"
    },
    {
        id: 3,
        title: 'Вы - не мёртвая душа',
        description: Text3,
        label: "corpse"
    },
    {
        id: 4,
        title: 'Документы',
        description: Text4,
        label: "documents"
    },
    {
        id: 5,
        title: 'Нет дисциплинарных взысканий',
        description: Text5,
        label: "no_disciplinary"
    }
];

import {IText} from "../types/IText";

export const FooterText: IText[] = [
    {
        id: 0,
        title: "Обратная связь",
        subtitle:"Вы можете связаться с нами, отправив сообщение"
    },

    {
        id: 1,
        title: "© 2022 Dormitory Management System | Powered by corpoReality",
        subtitle:""
    },
]
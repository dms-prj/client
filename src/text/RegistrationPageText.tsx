import {IText} from "../types/IText";

export const RegistationPageText: IText[] = [
    {
        id: 0,
        title: "Регистрация",
        subtitle:"Пользователь с таким e-mail уже существует"
    },

    {
        id: 1,
        title: "Имя",
        subtitle:""
    },

    {
        id: 2,
        title: "Email",
        subtitle:""
    },

    {
        id: 3,
        title: "Пароль",
        subtitle:""
    },

    {
        id: 4,
        title: "Уже есть аккаунт? Войти",
        subtitle: ""
    },
]
import {IText} from "../types/IText";

export const AuthorizationDialogText: IText[] = [
    {
        id: 0,
        title: "Вход",
        subtitle:"Неверный e-mail или пароль"
    },

    {
        id: 1,
        title: "Email",
        subtitle:""
    },

    {
        id: 2,
        title: "Пароль",
        subtitle:""
    },

    {
        id: 3,
        title: "Нет аккаунта? Зарегистрироваться",
        subtitle: ""
    },
]
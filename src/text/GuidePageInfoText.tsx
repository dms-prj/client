import {IText} from "../types/IText";

export const GuidePageInfoText: IText[] = [
    {
        id: 0,
        title: "Твой личный гайд по переселению",
        subtitle:"вся необходимая информация собрана в одном месте"
    },
    {
        id: 1,
        title: "Порядок переселения",
        subtitle: "Подробная инструкция по шагам, которые вам нужно сделать, чтобы успешно переселиться"
    }
]
import {IText} from "../types/IText";

export const FeedbackDialogText: IText[] = [
    {
        id: 0,
        title: "Обратная связь",
        subtitle:"Если вы обнаружили неточность или неактуальность информации, а также, если у вас есть идеи для развития продукта - сообщите нам об этом"
    },

    {
        id: 1,
        title: "Message",
        subtitle:"Вы помогаете нам стать лучше"
    },
]
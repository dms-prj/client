import React, {FC} from 'react';
import {Link, Typography} from "@mui/material";
import {TextProps} from "./TextProps";

const Text2: FC<TextProps> = ({sx}) => {
    return (
        <Typography sx={sx}>
            {"Получить заявление на переселение у коменданта или в отделе поселения (10 общежитие, 106 кабинет), или распечатать с "}<Link href="https://campus.spbu.ru/documents/peresel.pdf" underline="hover" color="secondary">сайтa кампуса</Link>.

            <p/>
            Возьмите с собой паспорт или студенческий билет.
        </Typography>
    );
};

export default Text2;
import React, {FC} from 'react';
import {Typography} from "@mui/material";
import {TextProps} from "./TextProps";


const Text0: FC<TextProps> = ( {sx}) => {
    return (
        <Typography sx={sx}>
            Заявки на переселение принимаются в течение всего года. Исключением являются август и сентябрь.
            <p />
            Если блок аварийный, то переселение возможно круглый год.
        </Typography>
    );
};

export default Text0;
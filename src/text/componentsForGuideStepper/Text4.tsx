import React, {FC} from 'react';
import {Typography} from "@mui/material";
import {TextProps} from "./TextProps";

const Text4: FC<TextProps> = ({sx}) => {
    return (
        <Typography sx={sx}>
            Следующий шаг - подать заявление в отдел поселения (10 общежитие, 106 кабинет), не забудьте взять паспорт или студенческий билет. После этого будет проведён ряд проверок:
            <ul>
                <li>проверка задолженностей об оплате</li>
                <p/>
                <li>проверка количества свободных мест и заявок на данное место</li>
                <p/>
                <li>проверка наличия действующей справки о прохождении флюорографии</li>
                <p/>
                <li>проверка отсутствия претензий по состоянию жилого помещения и мебели</li>
                <p/>
                <li>проверка отсутствия дисциплинарных взысканий</li>
                <p/>
                <li>проверка задолженностей по оплате дополнительных услуг</li>
                <p/>
                <li>проверка факта отсутствия в общежитии больше 30 дней подряд не в каникулярное время</li>
            </ul>
            В течение 5 рабочих дней вас оповестят о принятии решения либо по почте, либо по телефону. При желании можно уточнить у коменданта, получили ли вы ответ на заявление.
        </Typography>
    );
};

export default Text4;
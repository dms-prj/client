import {SxProps} from "@mui/material";

export interface TextProps {
    sx?: SxProps
}
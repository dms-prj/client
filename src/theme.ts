import { createTheme } from '@mui/material/styles';
import { ruRU } from '@mui/material/locale';

// A custom theme for this app
const theme = createTheme(
  {
    palette: {
      primary: {
          main: '#291FBF',
      },
      secondary: {
          main: '#645DCA',
      },
      divider: 'rgba(100, 93, 202, 0.1)',
      action: {
          active: 'rgba(100, 93, 202, 0.54)',
          hover: 'rgba(100, 93, 202, 0.04)',
          selected: 'rgba(100, 93, 202, 0.08)',
          focus: 'rgba(100, 93, 202, 0.12)'
      },
      info: {
        // woman room
        main: '#A367C9',
        // man room
        light: '#538AC9'
      }
    },
    typography: {
      h4: {
          color: '#201D40',
      },
      h5: {
          color: '#201D40',
      },
      h6: {
          color: '#201D40',
          fontWeight: 400,
      },
      subtitle1: {
          color: '#201D40',
      }
    },
  },
  ruRU,
);

export default theme;

import React, {FC} from 'react';
import {IGuideItem} from "../types/IGuideItem";
import {Box, SxProps} from "@mui/material";
import GuideStep from "./GuideStep";

interface GuideStepperProps{
    guideArray: IGuideItem[]
    sx?: SxProps
}

const GuideStepper: FC<GuideStepperProps> = ({guideArray, sx}) => {
    return (
        <Box sx={sx}>
            {guideArray.map( (guideItem, id) =>
                <GuideStep guideItem={guideItem} id={id + 1} isLast={id === guideArray.length - 1} key={guideItem.id}/>
            )}
        </Box>
    );
};

export default GuideStepper;
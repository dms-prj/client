import {FC, useEffect} from "react";
import { Card, CardContent, TablePagination, Pagination, Container } from "@mui/material";
import RoomItem from "./RoomItem";
import {campusApi, ListParams} from "../services/campus";
import { useAppDispatch, useAppSelector } from "../hooks/redux";
import {setPage, setPerPage, setPerPageNumber, setTotal} from "../store/reducers/pagesSlice";
import {click_search, setSearchState, stop_search} from "../store/reducers/searchSlice";
import {useLocation, useNavigate} from "react-router-dom";
import {setFilterState} from "../store/reducers/filterSlice";
import {makeListParams, makeURLsearchFromParams, parseUrlParamsToFilter} from "../services/parsing_url/parsing_url";

const RoomList: FC = () => {

    const location = useLocation();
    const dispatch = useAppDispatch();

    const { page, per_page, total, total_page } = useAppSelector(state => state.pagesReducer);

    const filter_params = useAppSelector(state => state.filterReducer);
    const {building, room} = useAppSelector(state => state.searchReducer);

    const submission_isStarted = useAppSelector(state => state.searchReducer.isClicked);

    const [ trigger, { isLoading, data: responseRooms, error }] = campusApi.useLazyListRoomsQuery();
    const navigate = useNavigate();

    // Сборка default фильтра из параметров url при первом рендеринге (перешли сюда по ссылке)
    useEffect(() => {
        const search = location.search;
        if (search.includes('?')) {
            const filter = parseUrlParamsToFilter(search);

            // настройку perPage берем из адресной строки, сначала ее - чтобы страница не обнулилась
            dispatch(setPerPageNumber(filter.perPage));
            // номер страницы берем из адресной строки
            dispatch(setPage(filter.page));

            // данные фильтра отправляем в store
            dispatch(setFilterState(filter.params!));
            dispatch(setSearchState({building: filter.building, room: filter.room}));
        }
        dispatch(click_search());
    }, [])

    // Обработка ОТПРАВКИ ЗАПРОСА (когда вызван click_search) и обновление параметров в url
    useEffect(() => {
        if (submission_isStarted) {
            // подготавливаем параметры для вызова API
            const listParams: ListParams = {
                limit: per_page,
                filter: {
                    page: page + 1,
                    perPage: per_page,
                    room: room,
                    building: building,
                    params: filter_params
                }
            };
            // вызываем API, обрабатываем возвращенный total
            trigger.call({}, listParams).unwrap().then((response) => {
                dispatch(setTotal(response.total));
            });
            dispatch(stop_search());

            // обновление url
            const search_params = new URLSearchParams(makeListParams(listParams));
            makeURLsearchFromParams(search_params);
            //console.log("search params: ", search_params.toString());
            navigate('?' + search_params.toString());
        }
    }, [submission_isStarted]);

    if (isLoading) {
        return <div>Loading</div>
    }

    if (!responseRooms || (!responseRooms!.rooms)) {
        return <div>No rooms :(</div>
    }

    const handleChangePage = (correct_page: number) => {
        dispatch(setPage(correct_page));
        dispatch(click_search());
    }

    const handleChangePerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        dispatch(setPerPage(event));
        dispatch(click_search());
    }

    return (
        <Container>
            <TablePagination
                component="div"
                count={total}
                page={page}
                rowsPerPageOptions={[5, 10, 25]}
                onPageChange={(event, page) => handleChangePage(page)}
                rowsPerPage={per_page}
                onRowsPerPageChange={(event) => handleChangePerPage(event)}
            />
            {responseRooms.rooms.map((item) =>(
                <RoomItem key={item.uid} room={item} />
            ))}
            <Card sx={{ borderRadius: 4,
                '& .MuiCardContent-root:last-child': { paddingBottom: 2 } }}
            >
                <CardContent>
                    <Pagination
                        count={total_page}
                        page={page + 1}
                        onChange={(event, page) => handleChangePage(page - 1)}
                        sx={{ display: "flex", justifyContent: "center" }}
                    />
                </CardContent>
            </Card>
        </Container>
    );
};

export default RoomList;
import React, {FC} from 'react';
import {IGuideItem} from "../types/IGuideItem";
import {Grid} from "@mui/material";
import GuideStepButton from "./UI/GuideStepperComponents/GuideStepButton";
import GuideStepConnector from "./UI/GuideStepperComponents/GuideStepConnector";
import GuideItemCard from "./UI/GuideStepperComponents/GuideItemCard";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

interface GuideStepPops{
    guideItem: IGuideItem
    id: number
    isLast: boolean
}

const GuideStep: FC<GuideStepPops> = ({guideItem, id, isLast}) => {
    
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));

    return (
        <Grid container direction="row" spacing={{ xs: "20px", md: "50px" }}>

            <Grid item>
                {matches ?
                <Grid container height="100%" flexDirection="column" display="flex">

                    <Grid item>
                        <GuideStepButton id={id} />
                    </Grid>
                    { (!isLast) ?
                        (id >= 4) ?
                            <Grid item flex="1" display="fles" justifyContent="center">
                                <GuideStepConnector />
                            </Grid>
                            :
                            (id < 3) ?
                                <Grid item flex="1" display="fles" justifyContent="center" padding="15px">
                                    <GuideStepConnector />
                                </Grid>
                                :
                                <Grid item flex="1" display="fles" justifyContent="center" paddingTop="15px">
                                    <GuideStepConnector />
                                </Grid>
                        :
                        null
                    }
                </Grid>
                : ''
                }
            </Grid>
            <Grid item>
                <GuideItemCard guideItem={guideItem} />
            </Grid>

        </Grid>
    );
};

export default GuideStep;
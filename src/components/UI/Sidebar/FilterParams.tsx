import { FC } from "react";
import { Card, Container } from "@mui/material";
import GenderFilter from "./GenderFilter";
import RoomFilter from "./RoomFilter";
import FreePlacesFilter from "./FreePlacesFilter";

const FilterParams: FC = () => {

    return (
        <Card sx={{ marginTop: 4, padding: 3, paddingTop: 2, borderRadius: 4 }}>
            <Container
                sx={{ border: '3px dashed', borderRadius: 2, borderColor: 'divider', px:2, py:1 }}
            >
                <GenderFilter />
                <RoomFilter />
                <FreePlacesFilter />                
            </Container>
        </Card>
    );
};

export default FilterParams;
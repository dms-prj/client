import { FormControl, FormLabel, RadioGroup, FormControlLabel, Radio } from "@mui/material";
import { FC } from "react";
import { useAppDispatch, useAppSelector } from "../../../hooks/redux";
import { changeTypeOfRoom } from "../../../store/reducers/filterSlice";
import {click_search} from "../../../store/reducers/searchSlice";


const RoomFilter: FC = () => {

    const value = useAppSelector(state => state.filterReducer.roomType);
    //console.log(value);
    const dispatch = useAppDispatch();

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(changeTypeOfRoom(event));
        dispatch(click_search());
    }

    return (
        <FormControl>
            <FormLabel id="room-type" sx={{ paddingTop: 2 }}>Тип комнаты</FormLabel>
            <RadioGroup
                aria-labelledby="room-type"
                name="room-type-radio-buttons-group"
                value={value}
                onChange={(event) => handleChange(event)}
            >
                <FormControlLabel value="2" control={<Radio />} label="Двухместная" />
                <FormControlLabel value="3" control={<Radio />} label="Трёхместная" />                    
                <FormControlLabel value="any-room-type" control={<Radio />} label="Любая" />                    
            </RadioGroup>
        </FormControl>
    );
};


export default RoomFilter;
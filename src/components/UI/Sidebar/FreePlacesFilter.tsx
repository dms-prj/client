import { FormControl, FormLabel, FormGroup, FormControlLabel, Checkbox } from "@mui/material";
import { FC } from "react";
import { useAppDispatch, useAppSelector } from "../../../hooks/redux";
import { chooseFreePlaces } from "../../../store/reducers/filterSlice";
import {click_search} from "../../../store/reducers/searchSlice";


const FreePlacesFilter: FC = () => {
    
    const { onePlace, twoPlaces, threePlaces } = useAppSelector(state => state.filterReducer.freePlaces);
    //console.log(onePlace, twoPlaces, threePlaces)
    const dispatch = useAppDispatch();

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(chooseFreePlaces(event));
        dispatch(click_search())
    }

    return (
        <FormControl>
            <FormLabel id="free-rooms" component="legend" sx={{ paddingTop: 2 }}>
                Число свободных мест в комнате
            </FormLabel>
            <FormGroup>
                <FormControlLabel
                    control={
                    <Checkbox 
                        checked={onePlace} 
                        onChange={(event) => handleChange(event)} name="onePlace"
                    />
                    }
                    label="1"
                />
                <FormControlLabel
                    control={
                    <Checkbox 
                        checked={twoPlaces} 
                        onChange={(event) => handleChange(event)} name="twoPlaces"
                    />
                    }
                    label="2"
                />
                <FormControlLabel
                    control={
                    <Checkbox 
                        checked={threePlaces} 
                        onChange={(event) => handleChange(event)} name="threePlaces"
                    />
                    }
                    label="3"
                />
            </FormGroup>
        </FormControl>
    );
};


export default FreePlacesFilter;
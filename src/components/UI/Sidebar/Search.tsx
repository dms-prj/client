import {FC, useEffect, useState} from "react";
import { TextField, Button, Card } from "@mui/material";
import {useAppDispatch, useAppSelector} from "../../../hooks/redux";
import {click_search, setBuilding, setRoom} from "../../../store/reducers/searchSlice";
import { setPage } from "../../../store/reducers/pagesSlice";


const Search: FC = () => {

    const dispatch = useAppDispatch();

    const room_init = useAppSelector(state => state.searchReducer.room);
    const building_init = useAppSelector(state => state.searchReducer.building);

    const [building, setBuildingValue] = useState<string>((building_init === undefined) ? "" : building_init);
    const [room, setRoomValue] = useState<string>((room_init === undefined) ? "" : room_init);
    const [isClick, setClick] = useState<boolean>(false);

    // установка значений из хранилища
    useEffect(() => {
        setBuildingValue((building_init === undefined) ? "" : building_init);
        setRoomValue((room_init === undefined) ? "" : room_init);
    }, [building_init, room_init])

    // обработка нажатия
    const handleClick = () => {
        dispatch(setBuilding(building));
        dispatch(setRoom(room));
        dispatch(setPage(0));
        dispatch(click_search());
        setClick(!isClick);
    };

    // обработка очистки полей поиска
    const handleClean = () => {
        setBuildingValue('');
        setRoomValue('');
        dispatch(setBuilding(''));
        dispatch(setRoom(''));
        dispatch(setPage(0));
        dispatch(click_search());
        setClick(!isClick);
    };

    return (
        <Card sx={{ padding: 3, paddingTop: 2, borderRadius: 4 }}>
            <TextField
                id="building"
                margin="normal"
                // required
                fullWidth
                multiline
                label="Номер общежития"
                placeholder="Например, 10"
                value={building}
                onChange={(e) => setBuildingValue(e.target.value)}
            />
            <TextField
                id="room"
                margin="normal"
                fullWidth
                label="Номер комнаты"
                // helperText="Необязательное поле"
                value={room}
                onChange={(e) => setRoomValue(e.target.value)}
            />
            <Button
                fullWidth
                variant="contained"
                disabled={!building && !room}
                sx={{ mt: 1 }}
                onClick={handleClick}
            >
                Найти
            </Button>
            {isClick &&
                <Button
                    fullWidth
                    variant="outlined"
                    sx={{ mt: 2 }}
                    onClick={handleClean}
                >
                    Очистить
                </Button>
            }
        </Card>
    );
};

export default Search;
import { FormControl, FormLabel, RadioGroup, FormControlLabel, Radio } from "@mui/material";
import { FC } from "react";
import { useAppDispatch, useAppSelector } from "../../../hooks/redux";
import { changeGender } from "../../../store/reducers/filterSlice";
import {click_search} from "../../../store/reducers/searchSlice";


const GenderFilter: FC = () => {
    
    const value = useAppSelector(state => state.filterReducer.gender);
    const dispatch = useAppDispatch();

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(changeGender(event));
        dispatch(click_search());
    }

    return (
        <FormControl>
            <FormLabel id="gender" sx={{ paddingTop: 2 }}>Пол</FormLabel>
            <RadioGroup
                aria-labelledby="gender"
                name="gender-radio-buttons-group"
                value={value}
                onChange={(event) => handleChange(event)}
            >
                <FormControlLabel value="male" control={<Radio />} label="Мужской" />
                <FormControlLabel value="female" control={<Radio />} label="Женский" />                    
                <FormControlLabel value="any-gender" control={<Radio />} label="Любой" />                    
            </RadioGroup>
        </FormControl>
    );
};


export default GenderFilter;
import { FC } from "react";
import { useAppSelector } from "../../../hooks/redux";
import { IconButton } from "@mui/material";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";

interface InfoButtonProps {
    labelId: string;
    isSelected: boolean;
}

const InfoButton: FC<InfoButtonProps> = ({labelId, isSelected}) => {    

    return(
        <IconButton 
            edge="end"
            aria-label={labelId}
            disabled
        >
            {isSelected ?
                <KeyboardArrowLeftIcon fontSize="large" color="secondary"/>
                : <KeyboardArrowRightIcon fontSize="large" color="action"/>
            }
        </IconButton>
    );
};

export default InfoButton;

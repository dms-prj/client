import { FC } from "react";
import { Button } from "@mui/material";
import {useAppDispatch} from "../../../hooks/redux";
import {openAuthDialog} from "../../../store/reducers/authorizationDialogSlice";


const LoginButton: FC = () => {

    const dispatch = useAppDispatch();

    const handleClick = () => {
        dispatch(openAuthDialog({}));
    }

    return(
        <Button 
            variant="outlined" 
            color="secondary"
            onClick={handleClick}
        >
            Войти
        </Button>
    );
};

export default LoginButton;

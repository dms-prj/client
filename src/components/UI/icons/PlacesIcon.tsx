import { FC, useState } from "react";
import { Typography, Popover } from "@mui/material";
import GroupIcon from '@mui/icons-material/Group';
import GroupsIcon from '@mui/icons-material/Groups';

interface PlacesIconProps {
    two: boolean;
    iconColor: string;
}; 

const PlacesIcon: FC<PlacesIconProps> = ({iconColor, two}) => {

    const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
    const open = Boolean(anchorEl);

    const handlePopoverOpen = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handlePopoverClose = () => {
        setAnchorEl(null);
    };
    
    return(
        <div>
            <Typography
                aria-owns={open ? 'mouse-over-popover' : undefined}
                aria-haspopup="true"
                onMouseEnter={handlePopoverOpen}
                onMouseLeave={handlePopoverClose}
            >
                {two ?
                    <GroupIcon sx={{ color: iconColor }}/>
                    :<GroupsIcon sx={{ color: iconColor }}/>
                }                
            </Typography>
            <Popover
                id="mouse-over-popover"
                sx={{ pointerEvents: 'none' }}
                PaperProps={{sx:{borderRadius: 10}}}
                open={open}
                anchorEl={anchorEl}
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                transformOrigin={{ vertical: 'top', horizontal: 'center', }}
                onClose={handlePopoverClose}
                disableAutoFocus
            >
            <Typography variant="body2" color={iconColor} sx={{ p: 1 }}>
                {two ? "двуxместная" : "трёхместная"}
            </Typography>
            </Popover>
        </div>
    );
};

export default PlacesIcon;
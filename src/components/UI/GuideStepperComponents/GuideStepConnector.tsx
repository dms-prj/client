import React, {FC} from 'react';
import {Box} from "@mui/material";

const GuideStepConnector: FC = () => {
    return (
        <Box height="100%" sx={{ width: "5px", borderRadius: "5px", backgroundColor: "rgba(100, 93, 202, 0.1)"}} />
    );
};

export default GuideStepConnector;
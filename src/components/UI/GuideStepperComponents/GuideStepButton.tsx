import React, {FC} from 'react';
import {Box, Typography} from "@mui/material";
import styles from "./GuideStepButton.module.css";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

interface GuideStepButtonProps {
    id: number,
    title?: string
}

const GuideStepButton: FC<GuideStepButtonProps> = ({id, title}) => {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));

    if (id >= 4 && matches) return (
        <Box className={styles.step_button_wide} display="flex" alignItems="center" justifyContent="center">
            <Typography> {id} </Typography>
        </Box>
    )
    else if (matches) return (
        <Box className={styles.step_button} display="flex" alignItems="center" justifyContent="center">
            <Typography> {id} </Typography>
        </Box>
    )
    else return (
        <Box display="flex" flexDirection="row" alignItems="center">
            <Box className={styles.mobile} display="flex" alignItems="center" justifyContent="center">
                <Typography> {id + 1} </Typography>
            </Box>
            {title}
        </Box>
    );
};

export default GuideStepButton;
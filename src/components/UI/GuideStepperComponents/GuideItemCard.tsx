import React, {FC} from 'react';
import {IGuideItem} from "../../../types/IGuideItem";
import {Card, CardHeader, CardContent, Divider, SxProps} from "@mui/material";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import GuideStepButton from './GuideStepButton';


interface GuideItemCardProps {
    guideItem: IGuideItem
}

const GuideItemCard: FC<GuideItemCardProps> = ({guideItem}) => {
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));

    const sx: SxProps = { padding: "8px 16px 8px 16px", border: "3px dashed", borderRadius: 2, borderColor: 'divider', color: "textSecondary"};

    return (
        <Card sx={{ width: { xs: "90%", md: "600px" }, marginBottom: { xs: 0, md: "40px"}, marginLeft: { xs: "5%" } }}>
            <CardHeader
                title={matches ? 
                    guideItem.title 
                    : <GuideStepButton id={guideItem.id} title={guideItem.title}/>}
                titleTypographyProps={{variant: matches ? "h5" : "h6"}}
                sx={{ padding: {xs: "10px 15px", md: "16px"} }}/>
            <Divider sx={{ borderColor: "rgba(100, 93, 202, 0.1)" }} />
            <CardContent sx={{ padding: {xs: "10px", md: "16px"} }}>
                { guideItem.content({sx}) }
            </CardContent>
        </Card>
    );
};

export default GuideItemCard;
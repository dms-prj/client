import { FC } from "react";
import { AppBar, Container, Toolbar } from "@mui/material";
import { Outlet } from "react-router-dom";
import NavTabs from "./NavTabs";
import UserName from "./UserName";
import AuthorizationDialog from "../../dialogs/AuthorizationDialog";
import LoginButton from "../button/LoginButton";
import { useAppSelector } from "../../../hooks/redux";


const Navbar: FC = () => {

    const isAuth = useAppSelector((state) => state.authorizationReducer.isAuth);
    const isDone = useAppSelector(state => state.authorizationReducer.isDone);

    return (
        <>
        <AppBar position="fixed" color="default">
            <Container maxWidth="xl">                
                <Toolbar variant="dense" sx={{ justifyContent: {xs: "center", md: "space-between"} }}>        
                    <NavTabs />
                    { (isAuth) ?
                        <UserName/> : <LoginButton/>
                    }                    
                </Toolbar>
            </Container>   
        </AppBar>
            <AuthorizationDialog/>
        <Outlet />
        </>
    );
};

export default Navbar;
import { FC, useState } from "react";
import { Menu, MenuItem, Box, Typography } from "@mui/material";
import {useAppDispatch, useAppSelector} from "../../../hooks/redux";
import {logout} from "../../../store/reducers/authorizationSlice";


const settings = ['Выйти'];

const UserName: FC = () => {

    const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);
    const userName = useAppSelector(state => state.authorizationReducer.userName);

    const dispatch = useAppDispatch();

    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };
    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };
    const handleClickMenuItem = () => {        
        dispatch(logout());
    };

    return (
        <Box sx={{ flexGrow: 0 }}>
            <Typography 
                component='button' 
                onClick={handleOpenUserMenu} 
                variant="body1"
                color='secondary'
                sx={{ 
                    border: 'none', 
                    backgroundColor: 'inherit',
                    cursor: 'pointer' 
                }}
            >
                {userName}
            </Typography>        
            <Menu
                sx={{ mt: 3, '& .MuiMenu-list': {paddingTop: 0, paddingBottom: 0 }}}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
                }}
                transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
            >
                {settings.map((setting) => (
                <MenuItem key={setting} onClick={handleClickMenuItem}>
                    <Typography 
                        textAlign="center" 
                        variant="button" 
                        color="secondary"
                    >
                        {setting}
                    </Typography>
                </MenuItem>
                ))}
            </Menu>
        </Box>
    );
};

export default UserName;
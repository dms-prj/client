import { FC, useEffect, useState } from "react";
import { Tab, Tabs } from "@mui/material";
import {NavLink, useLocation, useNavigate} from "react-router-dom";
import {useAppDispatch, useAppSelector} from "../../../hooks/redux";
import {AuthStatus} from "../../../store/reducers/authorizationSlice";
import {openAuthDialog} from "../../../store/reducers/authorizationDialogSlice";
import {loginAPI} from "../../../services/auth_and_registr";


const pages: string[] = ['', 'checklist', 'freerooms'];

const getValueFromLocation = (pathname: string) : number => {
    const page_name = pathname.split('/')[1].toLowerCase();
    return pages.indexOf(page_name);
}

const NavTabs: FC = () => {

    let location = useLocation();
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const [value, setValue] = useState(getValueFromLocation(location.pathname));

    const isAuth = useAppSelector((state) =>  state.authorizationReducer.isAuth);
    const authIsDone = useAppSelector(state => state.authorizationReducer.isDone);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        handleChangeFunc(newValue);
        window.scrollTo(0,0);
    }

    const handleChangeFunc = (newValue: number) => {
        if ((newValue === 1) && (!(isAuth))) {
            if (getValueFromLocation(location.pathname) === 1)
                navigate('/');
            dispatch(openAuthDialog({to: pages[newValue]}));
        }
        else {
            setValue(newValue);
            if (newValue === 1  && !location.pathname.split('/')[2])
                navigate(pages[newValue]);
        }
    }

    useEffect(() => {
        if (authIsDone) {
            const newValue = getValueFromLocation(location.pathname);
            handleChangeFunc(newValue);
        }
    }, [location.pathname, authIsDone, isAuth]);

    return(
        <Tabs
            value={value} 
            onChange={handleChange}
            aria-label="nav tabs" 
            variant="fullWidth"
        >
            <Tab label="порядок переселения" component={NavLink} to={`/${pages[0]}`}/>
            <Tab label="чек-лист"
                 sx={{
                     color: (isAuth) ?
                         "text.secondary"
                         :
                         "text.disabled"                         
            }}/>
            <Tab label="свободные места" component={NavLink} to={`/${pages[2]}`}/>                 
        </Tabs>
    );
};

export default NavTabs;
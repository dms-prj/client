import React, {FC, PropsWithChildren, ReactElement, useEffect} from 'react';
import {useAppDispatch, useAppSelector} from "../../hooks/redux";
import {AuthStatus} from "../../store/reducers/authorizationSlice";
import {openAuthDialog} from "../../store/reducers/authorizationDialogSlice";
import {Navigate, useNavigate} from "react-router-dom";
//import {pageOfEnum} from "../UI/Navbar/NavTabs";

interface RequireAuthProps {
    children: ReactElement;
}

const RequireAuth: ({children}: { children: any }) => React.ReactElement<any, string | React.JSXElementConstructor<any>> | null = ({children}) => {

    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const isAuth = useAppSelector(state =>  state.authorizationReducer);

    useEffect(() => {
        console.log(isAuth);
        /*if (!(isAuth === AuthStatus.AUTHORIZED)) {
            console.log("auth 1: ", isAuth)
            console.log("auth 1: ", AuthStatus.AUTHORIZED.toString())
            console.log("require auth and open dialog!")
            dispatch(openAuthDialog({to: "/checklist"}));
            navigate('/');
        }*/
    }, [])

    return (isAuth) ?
        children
        :
        (
        null
        );
};

export default RequireAuth;
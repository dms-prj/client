import { FC } from "react";
import { useParams } from "react-router-dom";
import { Card, CardContent} from "@mui/material";
import { checkListText as content } from "../text/checkListText";
import { useAppDispatch, useAppSelector } from "../hooks/redux";
import { choose } from "../store/reducers/checkItemSlice";


const InfoItem: FC = () => {
    const { id } = useParams<{ id: string }>();
    const isSelected = useAppSelector((state) => state.checkItemReducer.selected);    
    const dispatch = useAppDispatch();

    if (isSelected != Number(id)) {
        dispatch(choose(Number(id)));        
    };   

    return (        
        <Card sx={{ maxWidth: 496 }}>
            <CardContent>
                {content[Number(id)].description({})}
            </CardContent>                    
        </Card>
    );
};

export default InfoItem;
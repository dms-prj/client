import React, { FC, useState } from 'react';
import {
    Box,
    Dialog,
    DialogContent,
    DialogTitle,
    DialogActions,
    IconButton,
    TextField,
    Typography,
    Button
} from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';
import SendIcon from '@mui/icons-material/Send'
import {IText} from "../../types/IText";
import {FeedbackDialogText} from "../../text/FeedbackDialogText";
import {useAppDispatch, useAppSelector} from "../../hooks/redux";
import {closeFeedbackDialog} from "../../store/reducers/feedbackDialogSlice";
import { msgApi } from '../../services/msgs';

const FeedbackDialog: FC = () => {

    const textDialog: IText = FeedbackDialogText[0];
    const textMessage: IText = FeedbackDialogText[1];
    const textSendButton: string = "Отправить";

    const isOpen = useAppSelector((state) => state.feedbackDialogReducer.isOpen);
    const dispatch = useAppDispatch();    

    const [message, setMessage] = useState<string>('');
    const [addMsg, {}] = msgApi.useAddMsgMutation();

    const handleClose = () => {
        dispatch(closeFeedbackDialog());
    };

    const handleSend = async () => {
        if (message) {
            await addMsg({message: message}).unwrap();
            setMessage('');
            handleClose();
        }        
    };

    return (
        <Dialog 
            open={isOpen} 
            onClose={handleClose} 
            PaperProps={{sx: { width: "600px", paddingX: 2, paddingY: 1, borderRadius: "10px" }}}
        >
            <DialogTitle sx={{ m: 0, p: 2 }} >
                <IconButton
                    onClick={handleClose}
                    sx={{
                    position: 'absolute',
                    right: 8,
                    top: 8
                }}>
                    <CloseIcon />
                </IconButton>
                <Typography variant="h5" mb={1}> {textDialog.title} </Typography>
                <Typography variant="body2"> {textDialog.subtitle} </Typography>
            </DialogTitle>
            <DialogContent sx={{ padding: "16px"}}>
                <Box width="100%" sx={{ marginTop: "16px"}}>
                    <TextField
                        multiline
                        variant="outlined"
                        helperText={textMessage.subtitle}
                        label={textMessage.title}
                        value={message}
                        onChange={(e) => setMessage(e.target.value)}
                        sx={{ width: "100%"}} />
                </Box>
            </DialogContent>
            <DialogActions sx={{align: "left"}}>
                <Button
                    variant="contained"
                    sx={{ backgroundColor: "main"}}
                    onClick={handleSend}
                    size="medium"
                    endIcon={<SendIcon />}
                    disabled={!message}
                >
                    {textSendButton}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default FeedbackDialog;
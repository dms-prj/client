import React, {FC, useEffect} from 'react';
import {
    Box,
    Button,
    Dialog,
    DialogContent,
    DialogTitle, Divider,
    IconButton, Link,
    TextField,
    Typography
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import {IText} from "../../types/IText";
import {AuthorizationDialogText} from "../../text/AuthorizationDialogText";
import {useAppDispatch, useAppSelector} from "../../hooks/redux";
import {closeAuthDialog, redirectAuthDialog} from "../../store/reducers/authorizationDialogSlice";
import {useNavigate} from "react-router-dom";
import {
    AuthStatus,
    login_done,
    login_failed_already_exist,
    login_started,
    login_success
} from "../../store/reducers/authorizationSlice";
import {loginAPI, LoginRequest} from "../../services/auth_and_registr";

const AuthorizationDialog: FC = () => {

    const textDialog: IText = AuthorizationDialogText[0];
    const textEmail: IText = AuthorizationDialogText[1];
    const textPassword: IText = AuthorizationDialogText[2];
    const textSignUp: IText = AuthorizationDialogText[3];
    const textSigningButton: string = "Войти";

    const isOpen = useAppSelector((state) => state.authorizationDialogReducer.isOpen);
    const dispatch = useAppDispatch();
    const authStatus = useAppSelector((state) => state.authorizationReducer.isAuth);
    const url_to = useAppSelector((state) => state.authorizationDialogReducer.to);
    const isFailed = useAppSelector((state) => state.authorizationReducer.isFailed);

    const [trigger, {isError}] = loginAPI.useLoginMutation();

    const url_registration = "/registration";
    const navigate = useNavigate();

    const handleClose = () => {
        dispatch(closeAuthDialog());
    };

    const handleSignIn = async (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const result: LoginRequest = {
            username: data.get('email')!.toString(),
            password: data.get('password')!.toString(),
        }

        dispatch(login_started());
        trigger.call({}, result).unwrap().then((response) => {
            console.log("response: ", response);
            dispatch(login_success(response));
            dispatch(login_done());
            handleSuccessfulAuth();
        }).catch((error) => {
            console.log("error: ", error);
            // TODO проверка на ошибку существующего юзера
            if (true) {
                dispatch(login_failed_already_exist());
            }
            dispatch(login_done());
            // TODO неудачная авторизация
        });
    };

    const handleSuccessfulAuth = () => {
        handleClose();
        //if (location.state?.to) {
        //    navigate(location.state.to)
        //}
        if (!(url_to === undefined))
            navigate('/' + url_to);
    }

    const handleLinkToRegistrationPage = () => {
        dispatch(redirectAuthDialog());
        //const url_to = (location.state?.to) ? location.state.to : location.pathname;
        //navigate(url_registration, {state: {to: url_to, from: location.pathname}});
        navigate(url_registration);
    }

    return (
        <Dialog open={isOpen} onClose={handleClose} PaperProps={{sx: { width: "652px", padding:"35px 50px 30px 50px" }}}>

            <DialogTitle sx={{p: 0, m: 0, marginBottom: "16px"}} >
                <IconButton
                    onClick={handleClose}
                    sx={{
                        position: 'absolute',
                        size: "large",
                        right: "27px",
                        top: "35px",
                        p: 0
                    }}>
                    <CloseIcon />
                </IconButton>
                <Typography variant="h5"> {textDialog.title} </Typography>
                {(!(isError)) ?
                    null
                    :
                    <Typography variant="subtitle1" color="red"> {textDialog.subtitle} </Typography>
                }
            </DialogTitle>
            <DialogContent sx={{m: 0, p: 0}}>
                <Box display="flex" flexDirection="column" component="form" onSubmit={handleSignIn} width="100%" sx={{}}>
                    <Divider sx={{marginBottom: "24px"}}/>
                    <TextField
                        name="email"
                        type="email"
                        variant="outlined"
                        required
                        label={textEmail.title}
                        error={isFailed}
                        sx={{ width: "100%", marginBottom: "16px", borderColor: 'red !important'}} />
                    <TextField
                        name="password"
                        type="password"
                        variant="outlined"
                        required
                        label={textPassword.title}
                        error={isFailed}
                        sx={{ width: "100%"}} />
                    <Button
                        type="submit"
                        variant="contained"
                        color="secondary"
                        sx={{marginTop: "32px", marginBottom: "30px"}}
                        fullWidth
                        size="large"
                    >
                        {textSigningButton}
                    </Button>
                    <Link
                        align="right"
                        underline="hover"
                        color="secondary"
                        component="button"
                        onClick={handleLinkToRegistrationPage}
                    >
                        {textSignUp.title}
                    </Link>
                </Box>
            </DialogContent>
        </Dialog>
    );
};

export default AuthorizationDialog;
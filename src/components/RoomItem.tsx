import { FC } from "react";
import { Card, CardContent, Typography, Chip, Box } from "@mui/material";
import { IRoom } from "../types/IRoom";
import PlacesIcon from "./UI/icons/PlacesIcon";


interface RoomItemProps {
    room: IRoom;
};

const RoomItem: FC<RoomItemProps> = ({room}) => {

    let chipColor: string = "GrayText";
    let gender: string = room.gender;

    if (gender === "Женский") {
        chipColor = "info.main";
    } else if (gender === "Мужской") {
        chipColor = "info.light";
    } else {
        gender = "Пустой блок";
    };

    return (
        <Card sx={{ borderRadius: 4, paddingBottom: 0, marginBottom: 4,
            '& .MuiCardContent-root:last-child': {
                paddingBottom: 2,
            },}}
        >
            <CardContent 
                sx={{ display: "flex", flexDirection: "row", alignItems: "center", 
                      justifyContent: "space-between"}}
            >
                <Box display="flex" alignItems="center">
                    <Typography component="span" variant="subtitle1" color="secondary" sx={{ paddingRight: 1 }}>
                        {room.dormitory} общежитие /
                    </Typography>
                    <Typography variant="subtitle1">
                        {room.number}
                    </Typography>
                </Box>
                <Typography variant="body2">
                        {room.freespace === 1 ?
                            "1 свободное место" : `${room.freespace} свободных места`
                        }
                </Typography>
                <Box display="flex" alignItems="center">
                    <Chip 
                        label={gender}
                        variant="outlined" 
                        sx={{ color: chipColor, borderColor: chipColor, marginRight: 2 }}
                    />
                    <PlacesIcon two={room.capacity === 2} iconColor={chipColor}/>                   
                </Box>
            </CardContent>
        </Card>
    );
};

export default RoomItem;
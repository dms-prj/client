import { FC } from "react";
import { List, Card } from "@mui/material";
import { checkListText as content } from "../text/checkListText";
import CheckListItem from "./CheckListItem";
import { useGetCheckListQuery } from "../services/checklist";
import { useAppDispatch } from "../hooks/redux";
import { setCheckList } from "../store/reducers/checkItemSlice";

const CheckList: FC = () => {

    const {data: currentList, isSuccess} = useGetCheckListQuery();
    const dispatch = useAppDispatch();
    
    if (isSuccess) {
        dispatch(setCheckList(currentList))
    };

    return (
        <Card sx={{ width: '100%', maxWidth: 588}}>
            <List>
                {content.map(item =>                        
                    <CheckListItem checkItem={item} key={item.id}/>
                )}                   
            </List>
        </Card>
    );
};

export default CheckList;

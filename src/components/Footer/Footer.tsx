import React, {FC} from 'react';
import {Box, Button, Grid, Typography} from "@mui/material";
import {FooterText} from "../../text/FooterText";
import {IText} from "../../types/IText";
import {useAppDispatch} from "../../hooks/redux";
import {openFeedbackDialog} from "../../store/reducers/feedbackDialogSlice";
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import FeedbackDialog from "../dialogs/FeedbackDialog";

const Footer: FC = () => {
    
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('md'));

    const textConnect: IText = FooterText[0];
    const textPoweredBy: IText = FooterText[1];
    const textConnectButton: string = "Отправить сообщение";

    const dispatch = useAppDispatch();

    const handleSend = () => {
        dispatch(openFeedbackDialog());
    }

    return (
        <Box 
            display="flex" 
            flexDirection="column" 
            alignItems="center"
            mb={ {xs: 1, md: 2} } 
            mt={{xs: "10vh", md: "30vh"}}
            sx={{ overflow: "hidden" }}
        >
            
            <Grid
                container
                direction="row"
                display="flex"
                width="100%"
                maxWidth={{xs: "sm", md: "lg"}}
                sx={{paddingX: {xs: 1, md: 8}, marginBottom: {xs: 4, md: 8}}}
            >
                <Grid item
                      display="flex"
                      justifyContent="flex-start"
                      alignItems="center"
                >
                    <Typography variant="h6" color="white"> {textConnect.title} </Typography>
                </Grid>

                <Grid item
                      flex="1"
                      display="flex"
                >
                    <Grid
                        container
                        spacing="25px"
                        display="flex"
                        direction="row"
                        justifyContent="flex-end"
                    >
                        {matches ?
                            <Grid item
                                display="flex"
                                alignItems="center">
                                <Typography align={"left"} variant="body2" color="white" sx={{width: "184px", textAlign: {xs: "end"} }}> {textConnect.subtitle} </Typography>
                            </Grid>
                            : ''
                        }
                        <Grid item display="flex" justifyContent="flex-end">
                            <Button variant="contained" sx={{ backgroundColor: "main", maxWidth: {xs: "90%", md: "100%"}}} onClick={handleSend}> {textConnectButton} </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Box width="100%" sx={{ backgroundColor: "white", height: {xs: "1px", md: "2px"} }}/>
            <Typography color="white" textAlign="center" sx={{ marginTop: 2, marginX: {xs: 1} }}> {textPoweredBy.title} </Typography>
            <FeedbackDialog />
        </Box>
    );
};

export default Footer;
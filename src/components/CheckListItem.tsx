import { FC } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ListItem, ListItemButton, ListItemText, ListItemIcon, Checkbox } from "@mui/material";
import { ICheckListItem } from "../types/ICheckListItem";
import { check, choose } from "../store/reducers/checkItemSlice";
import { useAppDispatch, useAppSelector } from "../hooks/redux";
import InfoButton from "./UI/button/InfoButton";
import { checklistApi } from "../services/checklist";


interface CheckListItemProps {
    checkItem: ICheckListItem;
}

const CheckListItem: FC<CheckListItemProps> = ({checkItem}) => {

    const isChecked = useAppSelector((state) => state.checkItemReducer.checked)[checkItem.label];
    const isSelected = useAppSelector((state) => state.checkItemReducer.selected);
    const dispatch = useAppDispatch();
    const [update, {}] = checklistApi.useUpdateCheckListMutation();
    const navigate = useNavigate();

    const { id } = useParams<{ id: string }>();

    if ( !id ) {
        dispatch(choose(-1));        
    };
    
    const idx = checkItem.id;
    const labelId = `checkbox-list-label-${checkItem.id}`;
    const name: string = checkItem.label;

    const handleClick = () => {
        dispatch(choose(idx));
        if (isSelected === idx) {
            navigate("/checklist");
        }
        else {
            navigate(`/checklist/${idx}`);
        };
    };

    const handleCheck = async () => {
        await update({name: name, state: !isChecked}).unwrap();
        dispatch(check(checkItem.label));        
    };

    return (
        <ListItem
            key={idx}
            divider={idx !== 5}
            selected={isSelected === idx}
        >
            <ListItemIcon>
                <Checkbox
                    checked={isChecked}
                    onClick={handleCheck}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ 'aria-labelledby': labelId }}
                    sx={{ '& .MuiSvgIcon-root': { fontSize: 28 } }}
                />
            </ListItemIcon>
            <ListItemButton onClick={handleClick}>                
                <ListItemText 
                    id={labelId} 
                    primary={checkItem.title} 
                    sx={{'& .MuiListItemText-primary': {
                        color: (isChecked) ? 'GrayText' : 'textDisabled',
                        fontSize: 24
                        }}}/>
                    <InfoButton labelId={labelId} isSelected={isSelected === idx}/>
            </ListItemButton>
        </ListItem>
    );
};

export default CheckListItem;
